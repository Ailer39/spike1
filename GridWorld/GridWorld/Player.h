#pragma once
class Player
{
private:
	int xPos;
	int yPos;

public:
	Player(int x, int y);
	void Move(int x, int y);
	int GetXPos();
	int GetYPos();
};

