#pragma once

#include "Player.h"

class GameLogic
{
private:
	bool _isFinshed;
	Player *_player;
	void DrawGame();
	void CreateBoard();

public:
	GameLogic();
	~GameLogic();
	bool IsFinished();
	void MovePlayer(char input);
};

