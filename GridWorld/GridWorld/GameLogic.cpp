#include "GameLogic.h"
#include <iostream>
#include <string>


using namespace std;

const int maxX = 8;
const int maxY = 8;
string _board[maxX][maxY];


GameLogic::GameLogic()
{
	_isFinshed = false;
	CreateBoard();
	DrawGame();
}

GameLogic::~GameLogic()
{
}

bool GameLogic::IsFinished()
{
	return this->_isFinshed;
}

void GameLogic::CreateBoard()
{
	this->_player = new Player(2, 7);
	for (int y = 0; y < maxY; y++)
	{
		for (int x = 0; x < maxX; x++)
		{
			if (x == 0 ||
				y == 0 ||
				x == maxX - 1 ||
				y == maxY - 1)
			{
				_board[x][y] = "#";
			}
			else
			{
				_board[x][y] = " ";
			}
		}
	}


	_board[1][1] = "G";
	_board[3][1] = "D";
	_board[4][1] = "#";
	_board[5][1] = "D";
	_board[4][2] = "#";
	_board[4][3] = "#";
	_board[4][4] = "#";
	_board[4][5] = "#";
	_board[3][5] = "#";
	_board[5][5] = "#";
	_board[2][5] = "#";
	_board[4][5] = "#";
	_board[1][3] = "#";
	_board[2][3] = "#";
	_board[6][3] = "D";
	_board[2][7] = "S";
}

void GameLogic::DrawGame()
{
	for (int y = 0; y < maxY; y++)
	{
		for (int x = 0; x < maxX; x++)
		{
			if (y == _player->GetYPos() &&
				x == _player->GetXPos())
			{
				cout << "P";
			}
			else
			{
				cout << _board[x][y];
			}
		}

		cout << endl;
	}
}

void GameLogic::MovePlayer(char input)
{
	int targetX = _player->GetXPos();
	int targetY = _player->GetYPos();

	switch (tolower(input))
	{
	case 'n':
		targetY -= 1;
		break;
	case 's':
		targetY += 1;
		break;
	case 'e':
		targetX += 1;
		break;
	case 'w':
		targetX -= 1;
		break;
	}

	if (targetX >= maxX ||
		targetY >= maxY ||
		_board[targetX][targetY] == "#")
	{
		cout << "Invalid move" << endl;
	}
	else if (_board[targetX][targetY] == "G")
	{
		this->_isFinshed = true;
		cout << "Wow - you have discovered a large chest filled with GOLD coins!" << endl;
	}
	else if (_board[targetX][targetY] == "D")
	{
		this->_isFinshed = true;
		cout << "Arrrrgh... you have fallen down a pit." << endl;
	}
	else if (targetX != _player->GetXPos() ||
		targetY != _player->GetYPos())
	{
		_player->Move(targetX, targetY);
		DrawGame();
	}
}