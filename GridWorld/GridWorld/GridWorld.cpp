// GridWorld.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "GameLogic.h"

using namespace std;


int main()
{
	GameLogic* logic = new GameLogic();
	char input;


	while (!logic->IsFinished())
	{
		cin >> input;

		if (tolower(input) == 'q')
		{
			break;
		}

		logic->MovePlayer(input);
	}

	delete logic;
	system("pause");
	return 0;
}

