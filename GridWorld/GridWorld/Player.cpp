#include "Player.h"

Player::Player(int x, int y)
{
	this->Move(x, y);
}

void Player::Move(int x, int y)
{
	this->xPos = x;
	this->yPos = y;
}

int Player::GetXPos()
{
	return this->xPos;
}

int Player::GetYPos()
{
	return this->yPos;
}
